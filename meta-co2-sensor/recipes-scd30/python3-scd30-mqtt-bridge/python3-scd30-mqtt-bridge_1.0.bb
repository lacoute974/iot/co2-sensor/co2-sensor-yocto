SUMMARY = "SCD30 CO₂ sensor MQTT bridge"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=61b6594a5230a3e37f4a47190fdd67d1"

SRC_URI = " \
    file://scd30_mqtt_bridge.py \
    file://scd30-mqtt-bridge.service \
    file://LICENSE \
"

S = "${WORKDIR}"

inherit allarch python3-dir systemd

RDEPENDS:${PN} += "python3 python3-scd30 python3-paho-mqtt"

SYSTEMD_SERVICE:${PN} = "scd30-mqtt-bridge.service"

FILES:${PN} += "${PYTHON_SITEPACKAGES_DIR}/scd30_mqtt_bridge.py \
"

do_install:append() {
	install -d ${D}${PYTHON_SITEPACKAGES_DIR}
	install -m 0755 ${WORKDIR}/scd30_mqtt_bridge.py ${D}${PYTHON_SITEPACKAGES_DIR}

	install -d -m 0755 ${D}${systemd_unitdir}/system
	install    -m 0644 ${S}/scd30-mqtt-bridge.service ${D}${systemd_unitdir}/system
}
