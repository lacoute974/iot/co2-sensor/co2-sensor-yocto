import time
import json
from threading import Thread
from queue import Queue

import paho.mqtt.client as mqtt
from scd30_i2c import SCD30

MQTT_SERVER  = "laco2re.lacouture.org"
MQTT_PORT    = 1883
MQTT_TIMEOUT = 60
MQTT_TOPIC   = "co2_sensor/telemetry"
MQTT_QOS     = 1 

SCD30_INTERVAL = 2


def monitor_sensor(event_queue, interval=2):
    """ Read measurements from SCD30 periodically, place them onto event_queue.
    """
    scd30 = SCD30()
    
    scd30.set_measurement_interval(interval)
    scd30.start_periodic_measurement()
    
    time.sleep(interval)
    
    while True:
        if scd30.get_data_ready():
            m = scd30.read_measurement()
            if m is not None:
                now = time.time()
                co2, temp, rh = m
                print(f"{now}: CO2: {co2:.2f}ppm, temp: {temp:.2f}'C, rh: {rh:.2f}%")
                event_queue.put((now, co2, temp, rh))
            time.sleep(interval)
        else:
            time.sleep(0.2)

def publish_timeseries(event_queue, mqtt_server, mqtt_port, mqtt_topic, mqtt_qos):
    """ Publish events received from event_queue to mqtt_client.

    Events will be published as JSON payloads sent to specified topic and qos.
    """

    client = mqtt.Client()
    client.connect(mqtt_server, mqtt_port, mqtt_qos)

    client.loop_start()
    while True:
        timestamp, co2, temp, rh = event_queue.get()
        payload = {
            'timestamp': timestamp,
            'co2': co2,
            'temperature': temp,
            'rh': rh
        }
        client.publish(mqtt_topic,
                       payload=json.dumps(payload), 
                       qos=mqtt_qos)
        
event_queue = Queue()

capture_thread = Thread(target=monitor_sensor,
                        kwargs={'event_queue': event_queue,
                                'interval': SCD30_INTERVAL},
                        daemon=True)
capture_thread.start()

publish_timeseries(event_queue, MQTT_SERVER, MQTT_PORT, MQTT_TOPIC, MQTT_QOS)
