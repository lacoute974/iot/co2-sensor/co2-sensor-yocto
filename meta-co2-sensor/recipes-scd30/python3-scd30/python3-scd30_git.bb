SUMMARY = "SCD30 CO₂ sensor Python driver"
HOMEPAGE = "https://github.com/RequestForCoffee/scd30"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=61b6594a5230a3e37f4a47190fdd67d1"

SRC_URI = "git://github.com/RequestForCoffee/scd30.git;protocol=https;branch=master"

# Modify these as desired
PV = "0.0.5+git${SRCPV}"
SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git"

inherit setuptools3

RDEPENDS:${PN} += "python3-core python3-datetime python3-logging"
RDEPENDS:${PN} += "python3-smbus2"
